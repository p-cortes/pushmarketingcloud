package com.tres83.marketingcloudnotificaction


import android.app.Application
import android.content.Context
import com.salesforce.marketingcloud.MCLogListener
import com.salesforce.marketingcloud.MarketingCloudConfig
import com.salesforce.marketingcloud.MarketingCloudSdk
import com.salesforce.marketingcloud.notifications.NotificationCustomizationOptions

/**
 * Created by ICC.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize logging _before_ initializing the SDK to avoid losing valuable debugging information.
        MarketingCloudSdk.setLogLevel(MCLogListener.VERBOSE)
        MarketingCloudSdk.setLogListener(MCLogListener.AndroidLogListener())

        MarketingCloudSdk.init(this, MarketingCloudConfig.builder().apply {
            setApplicationId("98d74483-3376-4ec6-a161-5ec83aea21db")
            setAccessToken("WDvdgsyDTJOaqESmJCLG3cxD")
            setSenderId("194075380769")
            setMarketingCloudServerUrl("https://mc16y3xk-dkjxnm8kw0nwjxl9xb4.device.marketingcloudapis.com/")
            setMid("7295536")
            setNotificationCustomizationOptions(
                    NotificationCustomizationOptions.create(R.drawable.ic_notification_icon)
            )
            // Other configuration options
        }.build(this as Context)) { status ->
            // TODO handle initialization status
        }
    }
}